#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

int main(int argc, char **argv)
{


    ros::init(argc, argv, "camera");

    ros::NodeHandle n2;

    ros::Publisher camera_pub = n2.advertise<std_msgs::String>("camera", 1000);

    ros::Rate loop_rate(10);
    
    std_msgs::String msg;     

    std::string ss = "R";

    msg.data = ss;

    while (ros::ok())
    {



        ROS_INFO("sending the following camera information: R");

        camera_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}