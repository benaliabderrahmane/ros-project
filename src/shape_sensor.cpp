#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <sstream>
#include <time.h>

int main(int argc, char **argv)
{

    clock_t debut;
    clock_t fin;

    debut = clock();

    ros::init(argc, argv, "shape");

    ros::NodeHandle n3;

    ros::Publisher shape_pub = n3.advertise<std_msgs::Bool>("shape", 1000);

    ros::Rate loop_rate(10);

    std_msgs::Bool msg;
    msg.data = true;

    while (ros::ok())
    {

        ROS_INFO("sending the following flag for shape: true");

        shape_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}