#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <sstream>
#include <time.h>

int main(int argc, char **argv)
{

    clock_t debut;
    clock_t now;

    debut = clock();

    ros::init(argc, argv, "pos");

    ros::NodeHandle n1;

    ros::Publisher pos_pub = n1.advertise<std_msgs::Bool>("pos", 1000);

    ros::Rate loop_rate(10);

    std_msgs::Bool msg;

    msg.data = false;

    while (ros::ok())
    {

    now = clock();
        
        if ((float)(now-debut)/CLOCKS_PER_SEC*100.0>2)
        {
            msg.data = true;
            ROS_INFO("sending the following for position: true");
        }
        else 
        {
            ROS_INFO("sending the following for position: false");
        }

        

        pos_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}