#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <std_msgs/Int8.h>
#include <string>

bool flagPos = false;
bool flagShape = false;
std::string color= "";
std_msgs::Int8 msg2;




void posCallback(const std_msgs::Bool::ConstPtr& msg)
{
    flagPos = msg->data;
    if (flagPos == 1)
    {
        ROS_INFO("item detected!! checking shape");
        if (flagShape == 1)
        {
        ROS_INFO("shape is good!! checking color");
        if (color == "R")
        {
            ROS_INFO("item is Red, robot X should move!!");
            ROS_INFO("sending 2");
            msg2.data = 1;   
            color == "";
        }
        else if (color == "G")
        {
            ROS_INFO("item is Green, robot Y should move!!");
            ROS_INFO("sending 2");
            msg2.data = 2;   
            color == "";
        }
        else
        {
            color == "";
            msg2.data = 0;           
        }
        flagShape == 0;
        }
    flagPos == 0;  
    }
}

void shapeCallback(const std_msgs::Bool::ConstPtr& msg)
{
flagShape = msg->data;
}

void cameraCallback(const std_msgs::String::ConstPtr& msg)
{
color = msg->data.c_str();
}



int main(int argc, char **argv)
{
    

    ros::init(argc, argv, "my_application");

    ros::NodeHandle n;

    ros::Subscriber sub1 = n.subscribe("pos", 1000, posCallback);
    ros::Subscriber sub2 = n.subscribe("camera", 1000, cameraCallback);
    ros::Subscriber sub3 = n.subscribe("shape", 1000, shapeCallback);
    
    ros::Publisher robot_pub = n.advertise<std_msgs::Int8>("robot", 1000);
    
    ros::Rate loop_rate(10);

    ROS_INFO("starting the application");

    while (ros::ok())
    {
        robot_pub.publish(msg2);

        ros::spinOnce();

        loop_rate.sleep();
    }
    ros::spin();


  return 0;
}