# ros project

application node for ros project

# application node:
## information about application node:
the node application added with three other nodes for sensors (for shape, position and camera).
- the sensor nodes send publish randomly a message in the topic, the message is either bool or char depending on the node.
- the application node has 3 subscriber; one for each sensor node and one publisher to give the information to the trajectory generation and control on which robot to move.
## important:
the code was tested separately from the application package and a testing video is provided, link to the separate package is in my gitlab profile:  https://gitlab.com/benaliabderrahmane
## how it works: 
the node check if there is an item present by reading the message from the position sensor, if there an item it then check its shape via the shape sensor if it is good it finally check if the item is red or green or other to make a decision on which robot to move, the final decision is published in a topic named robot with an integer value:
- 0 for none.
- 1 for robot y.
- 2 for robot x.
